package com.example.provaPDM

import java.text.DecimalFormat

data class Vehicle(val model : String, var price : Float, val type: VehicleType, var sold : Boolean = false ){

    var status : String = ""

    init {
        setStatus()
    }

    fun setStatus(){
        if (!sold) {
            status = "This vehicle is available"
        }else
        {
            status = "This vehicle is not available anymore"
        }
    }

    fun formatPrice() : String{
        val formatter = DecimalFormat("#,###,###.00");
        return formatter.format(price/100)
    }


}
