package com.example.provaPDM

import androidx.compose.runtime.mutableStateListOf

object DAOVehicle{
    var vehicles = mutableStateListOf<Vehicle>()

    init {
        val vehicle = Vehicle("Uno", price = 12_000_00F, VehicleType.HATCH)
        saveVehicle(vehicle)
    }

    fun saveVehicle(vehicle: Vehicle){
        vehicles.add(vehicle)
    }

    fun getVehicles() : List<Vehicle>{
        return vehicles
    }
}