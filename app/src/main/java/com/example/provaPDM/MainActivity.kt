package com.example.provaPDM

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.*
import androidx.compose.animation.core.tween
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.modifier.modifierLocalOf
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.unit.toSize

class MainActivity : ComponentActivity() {

    private val viewModel by viewModels<ExpandableListViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            // Calling the composable function
            // to display element and its contents
            MainContent(viewModel)
        }
    }
}

// Creating a composable
// function to display Top Bar
@Composable
fun MainContent(viewModel: ExpandableListViewModel) {
    Scaffold(
        topBar = { TopAppBar(title = { Text("Register New Car", color = Color.White) }, backgroundColor = Color(0xFF0F5F9D)) },
        content = { MyContent(viewModel = viewModel)}
    )
}

@Composable
fun ListofVehicles(viewModel: ExpandableListViewModel) {

    val itemIds by viewModel.itemIds.collectAsState()

    Column(modifier = Modifier.clip(RoundedCornerShape(10))) {
        LazyColumn() {
            itemsIndexed(viewModel.getData()) { index, item ->
                ExpandableContainerView(
                    itemModel = item,
                    onClickItem = { viewModel.onItemClicked(index) },
                    expanded = itemIds.contains(index),
                )
            }
        }
    }
}

@Composable
fun ExpandableContainerView(itemModel: Vehicle, onClickItem: () -> Unit, expanded: Boolean) {
    Box(
        modifier = Modifier
            .background(Color(0xFFECECEC))
    ) {
        Column {
            HeaderView(vehicle = itemModel, onClickItem = onClickItem)
            ExpandableView(vehicle = itemModel, isExpanded = expanded)
        }
    }
}

@Composable
fun ExpandableView(vehicle: Vehicle, isExpanded: Boolean) {
    // Opening Animation
    val expandTransition = remember {
        expandVertically(
            expandFrom = Alignment.Top,
            animationSpec = tween(300)
        ) + fadeIn(
            animationSpec = tween(300)
        )
    }

    // Closing Animation
    val collapseTransition = remember {
        shrinkVertically(
            shrinkTowards = Alignment.Top,
            animationSpec = tween(300)
        ) + fadeOut(
            animationSpec = tween(300)
        )
    }

    AnimatedVisibility(
        visible = isExpanded,
        enter = expandTransition,
        exit = collapseTransition
    ) {
        Column() {

            Row() {
                Text(text = "  Type: ",
                    fontSize = 16.sp)

                Text(
                    text = vehicle.type.type,
                    fontSize = 16.sp,
                    color = Color.Black,
                    modifier = Modifier
                        .fillMaxWidth()
                )
            }

            Row() {
                Text(text = "  Price: ",
                    fontSize = 16.sp)

                Text(
                    text = vehicle.formatPrice(),
                    fontSize = 16.sp,
                    color = Color.Black,
                    modifier = Modifier
                        .fillMaxWidth()
                )
            }

            Row() {
                Text(text = "  Status: ",
                    fontSize = 16.sp)

                Text(
                    text = vehicle.status,
                    fontSize = 16.sp,
                    color = Color.Black,
                    modifier = Modifier
                        .fillMaxWidth()
                )
            }
            
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun HeaderView(vehicle: Vehicle, onClickItem: () -> Unit) {
    Box(
        modifier = Modifier
            .combinedClickable(
                //indication = null, // Removes the ripple effect on tap
                //interactionSource = remember { MutableInteractionSource() }, // Removes the ripple effect on tap
                onClick = onClickItem,
                onLongClick = {
                    vehicle.sold = true
                    vehicle.setStatus()
                }
            )

    ) {
        Row(modifier = Modifier.height(40.dp)){
            Text(
                text = "",
                modifier = Modifier
                    .fillMaxHeight()
                    .width(10.dp)
                    .background(
                        if (!vehicle.sold)
                            Color(0xFF4CAF50)
                        else
                            Color(0xFFF44336)
                    )
            )

            Text(
                text = vehicle.model,
                fontSize = 17.sp,
                color = Color.Black,
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight()
                    .padding(10.dp, 10.dp),
                textDecoration = if (vehicle.sold)
                    TextDecoration.LineThrough
                else
                    TextDecoration.None
            )

            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                Text(
                    text =
                    if (vehicle.sold)
                        "Sold"
                    else
                        "",
                )
            }

        }
    }
}

// Creating a composable function
// to create an Outlined Text Field
// Calling this function as content
// in the above function
@Composable
fun MyContent(viewModel: ExpandableListViewModel){

    // Declaring a boolean value to store
    // the expanded state of the Text Field
    var mExpanded by remember { mutableStateOf(false) }

    var model by remember{ mutableStateOf("")}
    var type by remember { mutableStateOf("") }
    var price by remember { mutableStateOf("")}

    // Create a list of cities
    val mVehicleTypes = VehicleType.getList()
    // Create a string value to store the selected city


    var mTextFieldSize by remember { mutableStateOf(Size.Zero)}

    // Up Icon when expanded and down icon when collapsed
    val icon = if (mExpanded)
        Icons.Filled.KeyboardArrowUp
    else
        Icons.Filled.KeyboardArrowDown


    Column(Modifier.padding(20.dp)) {

        OutlinedTextField(
            value = model,
            onValueChange = { model = it },
            label = {Text(text = "Model")},
            modifier = Modifier.fillMaxWidth()
        )

        Box{
            // Create an Outlined Text Field
            // with icon and not expanded
            OutlinedTextField(
                value = type,
                onValueChange = { type = it },
                readOnly = true,
                modifier = Modifier
                    .fillMaxWidth()
                    .onGloballyPositioned { coordinates ->
                        // This value is used to assign to
                        // the DropDown the same width
                        mTextFieldSize = coordinates.size.toSize()
                    },
                label = {Text("Type")},
                trailingIcon = {
                    Icon(icon,"contentDescription",
                        Modifier.clickable { mExpanded = !mExpanded })
                }
            )

            // Create a drop-down menu with list of cities,
            // when clicked, set the Text Field text as the city selected
            DropdownMenu(
                expanded = mExpanded,
                onDismissRequest = { mExpanded = false },
                modifier = Modifier
                    .width(with(LocalDensity.current){mTextFieldSize.width.toDp()})
            ) {
                mVehicleTypes.forEach { label ->
                    DropdownMenuItem(onClick = {
                        type = label
                        mExpanded = false
                    }) {
                        Text(text = label)
                    }
                }
            }
        }

            OutlinedTextField(
                modifier = Modifier.fillMaxWidth(),
                value = price,
                onValueChange = {
                    price = if (it.startsWith("0")) {
                        ""
                    } else {
                        it
                    }
                },
                label = { Text("Price") },
                visualTransformation = CurrencyAmountInputVisualTransformation(
                    fixedCursorAtTheEnd = true
                ),
                keyboardOptions = KeyboardOptions( keyboardType = KeyboardType.NumberPassword)
            )

        Button(
            onClick = {
                val vehicle = Vehicle(model, price.toFloat(), VehicleType.valueOf(type.uppercase().replace("\\s".toRegex(), "_")))
                viewModel.add(vehicle)
                      },
            colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF0F5F9D)),
            modifier = Modifier.align(Alignment.CenterHorizontally)
        )
        {
            Text(text = "Submit", color = Color(0xFFFFFFFF))
        }

        //Here is my list
        ListofVehicles(viewModel = viewModel)

    }
}


// For displaying preview in
// the Android Studio IDE emulator
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    val viewModel = ExpandableListViewModel()
    MainContent(viewModel = viewModel)
}